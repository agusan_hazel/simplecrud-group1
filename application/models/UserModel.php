<?php
class UserModel extends CI_Model
{
    public function insert($data)
    {
        return $this->db->insert('user',$data);
    }
    public function read()
    {
        return $this->db->get('user')
                        ->result_array();
    }
    public function getById($id)
    {
        return $this->db->where('id',$id)
                    ->get('user')
                    ->row_array();
    }
    public function updateRecord($id,$data)
    {
       return $this->db->where('id',$id)
                    ->update('user',$data);
    }
    public function deleteRecord($id)
    {
       return $this->db->where('id',$id)
                        ->delete('user');
    }
}
?>