<?php
class User extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->Model('UserModel');
  }
  public function index()
  {
    $data['users']=$this->UserModel->read();
    $this->load->view('user/index',$data);
  }
   public function add()
   {
     $this->load->view('user/add');
   }
   public function save()
   {
     $post=$this->input->post();
     $res=$this->UserModel->insert($post);
     if($res)
     {
       $data['value']=1;
        $data['msg']='record added successfully';
     }
     else
     {
        $data['value']=0;
        $data['msg']='record added failed';
     }
     $this->load->view('user/add',$data);
   }
   public function edit($id)
   {
      $data['user_edit']=$this->UserModel->getById($id);
      $this->load->view('user/edit',$data);
   }
   public function update($id)
   {
      $post=$this->input->post();
      $res=$this->UserModel->updateRecord($id,$post);
      if($res)
     {
       $data['value']=1;
        $data['msg']='record Updated successfully';
     }
     else
     {
        $data['value']=0;
        $data['msg']='record Update failed';
     }
     $data['users']=$this->UserModel->read();
     $this->load->view('user/index',$data);
   }
   public function delete($id)
   {
      $res=$this->UserModel->deleteRecord($id);
      if($res)
     {
       $data['value']=1;
        $data['msg']='record Deleted successfully';
     }
     else
     {
        $data['value']=0;
        $data['msg']='record Delete failed';
     }
     $data['users']=$this->UserModel->read();
     $this->load->view('user/index',$data);
   }

}
?>