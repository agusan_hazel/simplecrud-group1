<!DOCTYPE HTML>
<html>
<head>
    <title>Codeginiter Basic CRUD Application</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
</head>
<body>
    <div class="container">
        <br>
        <?php if(isset($value)&&isset($msg)) {?>
        <div class="alert alert-<?=($value==1)?'success':'danger'?>">
            <?=$msg?>
        </div>
        <?php }?>
        <div class="page-header">
            <h1>Update User</h1>
        </div>
        <form action="<?=base_url()?>index.php/user/update/<?=$user_edit['id']?>" method="post">
            First Name
            <input type='text' name='first_name' class='form-control' value="<?=$user_edit['first_name']?>" />
            <br>
            Last Name
            <input type='text' name='last_name' class='form-control' value="<?=$user_edit['last_name']?>" />
            <br>
            Email
            <input type='email' name='email' class='form-control' value="<?=$user_edit['email']?>" />
            <br>
            Password
            <input type='password' name='password' class='form-control' value="<?=$user_edit['first_name']?>" />
            <br>
            <input type='submit' value='Update' class='btn btn-primary' />
            <a href="<?=base_url()?>" class='btn btn-danger'>Back to User List</a>
        </form>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>