<!DOCTYPE HTML>
<html>

<head>
    <title>Codeginiter CRUD Application</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

</head>

<body>
    <div class="container">
    <?php if(isset($value)&&isset($msg)) {?>
        <div class="alert alert-<?=($value==1)?'success':'danger'?>">
            <?=$msg?>
        </div>
        <?php }?>
        <br>
        <div class="page-header">
            <h1>Users List</h1>
        </div>
        <br />
        <a href="<?=base_url()?>index.php/user/add" class='btn btn-primary m-b-1em'>Add New User</a>
        <br />
         <table class='table table-hover table-responsive table-bordered'>
            <br />
            <?php if(count($users)>0){?>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            <tbody>
                <?php $count=1;
                foreach($users as $user){ ?>
                <tr>
                    <td> <?= $count++ ?> </td>
                    <td> <?= $user['first_name'] ?> </td>
                    <td> <?= $user['last_name'] ?> </td>
                    <td> <?= $user['email'] ?> </td>
                    <td>
                        <a href="<?=base_url()?>index.php/user/edit/<?=$user['id']?>" class='btn btn-primary m-r-1em'>Edit</a>

                        <a href="<?=base_url()?>index.php/user/delete/<?=$user['id']?>" onclick='return confirm("Do you want to delete")'
                            class='btn btn-danger'>Delete</a>
                    </td>
                </tr>

            </tbody>
            <?php } }
        else{
            echo "<div class='alert alert-danger'>No records found.</div>";
        }
        ?>
        </table>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>

</html>